import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/login'
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  // {
  //   path: '/active',
  //   name: 'Home',
  //   component: Home,
  //   meta: {
  //     keepAlive: true
  //   }
  // },
  // {
  //   path: '/completed',
  //   name: 'Home',
  //   component: Home,
  //   meta: {
  //     keepAlive: true
  //   }
  // }
]

const router = new VueRouter({
  routes
})

export default router
